﻿using UnityEngine;
using System.Collections;

public class Player : MonoBehaviour {
    public float speed;
    public float runningSpeed;
    public float slideForce;
    public float checkDist = 3.0f;
    public float jumpBonus = 0.0f;
    public float gravityDefault = 1.5f;
    public float gravityFalling = 6.5f;

    public int jumpStrength;

    private Rigidbody2D rb;
    private Animator anim;

    

    private float speedMod = 1.0f;
    private bool grounded;
    private bool running;
    private bool attacking = false; 
    // Use this for initialization
    void Start () 
    {
        rb = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();

	}
	
	// Update is called once per frame
	void Update () 
    {
        
        
        
	}

    void FixedUpdate()
    {
        CheckGrounded();
        //Horizontal Movement
        float xMove = Input.GetAxisRaw("Horizontal");

        if(xMove < 0)
        {
            GetComponent<SpriteRenderer>().flipX = true;
        }

        if(xMove > 0)
        {
            GetComponent<SpriteRenderer>().flipX = false;

        }

        if(rb.velocity.y < 0)
        {
            rb.gravityScale = gravityFalling;
        }
        anim.SetFloat("XSpeed", Mathf.Abs(xMove * speed));
        Vector2 totalMovement = new Vector2(xMove * speed * speedMod, 0);
        rb.AddForce(totalMovement);

        HandleJump();
        HandleRun(xMove);
        //HandleSlide();
        //HandleFlight();

    }

    private void CheckGrounded()
    {
        //Ground Check  
        Vector2 checkPos = new Vector2(transform.position.x, transform.position.y - GetComponent<BoxCollider2D>().bounds.size.y / 2.0f);

        RaycastHit2D groundCheck = Physics2D.Raycast(checkPos, checkDist * -transform.up, checkDist);
        Debug.DrawRay(checkPos, -transform.up * checkDist, Color.green);

        if (groundCheck.collider != null)
        {
            float dotProductCheck = Vector2.Dot(-transform.up, groundCheck.transform.up);

            if (dotProductCheck <= -1.0f)
            {
                grounded = true;
                rb.gravityScale = gravityDefault;
                jumpBonus = 0.0f;
            }
        }

        else
        {
            grounded = false;

        }
        anim.SetBool("Grounded", grounded);

        Debug.Log(grounded);

    }
    private void HandleJump()
    {
        //Jumping
        if (Input.GetButtonDown("Jump") && grounded)
        {

            if (anim.GetBool("Running"))
                jumpBonus += 30.0f;

            Vector2 jump = new Vector2(rb.velocity.x, jumpStrength + jumpBonus);
            anim.SetTrigger("Jump");

            rb.AddForce(jump);
            
        }
    }

    private void HandleRun(float xMove)
    {
        //Running
        if (Input.GetButtonDown("RunButton") && xMove != 0)
        {
            speedMod = runningSpeed;
            running = true;
            anim.SetBool("Running", running);
        }

        if (Input.GetButtonUp("RunButton"))
        {
            speedMod = 1.0f;
            running = false;
            anim.SetBool("Running", running);
        }
    }

    private void HandleSlide()
    {
        //Slide!
        if (Input.GetButtonDown("SlideButton") && grounded)
        {
            if (GetComponent<SpriteRenderer>().flipX)
                rb.velocity = new Vector3(-slideForce, rb.velocity.y, 0);

            if (!GetComponent<SpriteRenderer>().flipX)
                rb.velocity = new Vector3(slideForce, rb.velocity.y, 0);
            anim.SetTrigger("Slide");
            jumpBonus = 5.0f;
        }
    }

    //Not sure on this yet.
    private void HandleFlight()
    {
        if (Input.GetButtonDown("FlightButton") && !grounded)
        {
            rb.gravityScale = 0.5f;
        }

        if (Input.GetButtonUp("FlightButton"))
        {
            rb.gravityScale = gravityFalling;
        }
    }

}
